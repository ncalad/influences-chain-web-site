# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171201155758) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "campaign_influencers", force: :cascade do |t|
    t.integer  "campaign_id"
    t.integer  "user_id"
    t.integer  "state",       default: 0
    t.boolean  "referred"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.datetime "deleted_at"
    t.string   "contract"
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "project_name"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "tenant_id"
    t.boolean  "start_auto"
    t.boolean  "end_auto"
    t.integer  "location"
    t.boolean  "use_tenant_logo"
    t.integer  "tenant_logo_placement"
    t.text     "terms"
    t.string   "contract"
    t.text     "welcome_text"
    t.string   "fulfilment_email"
    t.text     "fulfilment_text"
    t.boolean  "unlimited"
    t.integer  "limit"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.datetime "deleted_at"
    t.boolean  "use_forum"
    t.index ["deleted_at"], name: "index_campaigns_on_deleted_at", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "product_id"
    t.string   "logo"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["product_id"], name: "index_categories_on_product_id", using: :btree
    t.index ["user_id"], name: "index_categories_on_user_id", using: :btree
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "first_name"
    t.string   "surname"
    t.string   "matrimonial_surname"
    t.string   "title"
    t.integer  "language"
    t.string   "email"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "parent_id"
    t.string   "type"
  end

  create_table "forums", force: :cascade do |t|
    t.string  "title"
    t.integer "campaign_id"
    t.index ["campaign_id"], name: "index_forums_on_campaign_id", using: :btree
  end

  create_table "logos", force: :cascade do |t|
    t.string   "image"
    t.integer  "placement"
    t.integer  "campaign_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "personality_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "topic_id"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["topic_id"], name: "index_posts_on_topic_id", using: :btree
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.integer  "forum_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["forum_id"], name: "index_products_on_forum_id", using: :btree
    t.index ["user_id"], name: "index_products_on_user_id", using: :btree
  end

  create_table "professions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "replies", force: :cascade do |t|
    t.text     "text"
    t.integer  "user_id"
    t.integer  "ticket_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ticket_id"], name: "index_replies_on_ticket_id", using: :btree
    t.index ["user_id"], name: "index_replies_on_user_id", using: :btree
  end

  create_table "social_networks", force: :cascade do |t|
    t.integer "user_id"
    t.integer "kind"
    t.string  "url"
    t.index ["user_id"], name: "index_social_networks_on_user_id", using: :btree
  end

  create_table "task_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "name"
    t.string   "summary"
    t.text     "description"
    t.integer  "user_id"
    t.integer  "task_type"
    t.integer  "campaign_id"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "survey"
    t.datetime "due"
  end

  create_table "tenants", force: :cascade do |t|
    t.string   "name"
    t.boolean  "client_list_available"
    t.string   "logo"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_tenants_on_deleted_at", using: :btree
  end

  create_table "tickets", force: :cascade do |t|
    t.string   "email"
    t.text     "text"
    t.boolean  "closed",                 default: false
    t.integer  "tenant_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "campaign_influencer_id"
    t.index ["campaign_influencer_id"], name: "index_tickets_on_campaign_influencer_id", using: :btree
    t.index ["tenant_id"], name: "index_tickets_on_tenant_id", using: :btree
  end

  create_table "topics", force: :cascade do |t|
    t.string   "subject"
    t.integer  "category_id"
    t.boolean  "closed",      default: false
    t.integer  "referred_id"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["category_id"], name: "index_topics_on_category_id", using: :btree
    t.index ["user_id"], name: "index_topics_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "last_login_at"
    t.string   "first_name"
    t.string   "surname"
    t.string   "matrimonial_surname"
    t.integer  "primary_language"
    t.string   "email"
    t.string   "avatar"
    t.string   "first_address"
    t.string   "phone_number"
    t.integer  "phone_type"
    t.integer  "gender"
    t.integer  "birth_year"
    t.integer  "marital_status"
    t.integer  "childrens"
    t.string   "profession"
    t.boolean  "key_influencer"
    t.boolean  "friends_and_family"
    t.text     "social_accouns"
    t.string   "type"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "locale",                    default: 0
    t.datetime "deleted_at"
    t.string   "password_token"
    t.datetime "password_token_expires_at"
    t.string   "second_address"
    t.string   "third_address"
    t.integer  "country"
    t.string   "city"
    t.integer  "postal_code"
    t.string   "interior"
    t.string   "personality_type"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "title"
    t.integer  "parent_id"
    t.index ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  end

  add_foreign_key "categories", "products"
  add_foreign_key "categories", "users"
  add_foreign_key "forums", "campaigns"
  add_foreign_key "posts", "topics"
  add_foreign_key "posts", "users"
  add_foreign_key "products", "forums"
  add_foreign_key "products", "users"
  add_foreign_key "replies", "tickets"
  add_foreign_key "replies", "users"
  add_foreign_key "social_networks", "users"
  add_foreign_key "tickets", "campaign_influencers"
  add_foreign_key "tickets", "tenants"
  add_foreign_key "topics", "categories"
  add_foreign_key "topics", "users"
end
