class CreateCampaignInfluencers < ActiveRecord::Migration[5.0]
  def change
    create_table :campaign_influencers do |t|
      t.integer :campaign_id
      t.integer :user_id
      t.integer :state, default: 0
      t.boolean :referred

      t.timestamps
    end
  end
end
