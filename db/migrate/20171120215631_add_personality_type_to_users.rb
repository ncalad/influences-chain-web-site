class AddPersonalityTypeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :personality_type, :string
  end
end
