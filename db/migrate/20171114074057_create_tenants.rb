class CreateTenants < ActiveRecord::Migration[5.0]
  def change
    create_table :tenants do |t|
      t.string :name
      t.boolean :client_list_available
      t.string :logo
      t.integer :user_id

      t.timestamps
    end
  end
end
