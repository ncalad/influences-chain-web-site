class AddUseForumToCampaigns < ActiveRecord::Migration[5.0]
  def change
    add_column :campaigns, :use_forum, :boolean
  end
end
