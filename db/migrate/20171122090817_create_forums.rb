class CreateForums < ActiveRecord::Migration[5.0]
  def change
    create_table :forums do |t|
      t.string :title
      t.references :campaign, foreign_key: true
    end
  end
end
