class CreateSocialNetworks < ActiveRecord::Migration[5.0]
  def change
    create_table :social_networks do |t|
      t.references :user, foreign_key: true
      t.integer :kind
      t.string :url
    end
  end
end
