module CampaignsHelper
  def tenants_options
    Tenant.all.map { |t| [t.name, t.id] }
  end

  def placement_class(placement)
    placement&.downcase&.gsub(' ', '-')
  end
end
