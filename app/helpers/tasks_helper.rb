module TasksHelper
  EVERYTHING_AFTER_SCRIPT = /(?<=<\/script>).*/

  def embed_survey(survey)
    survey&.gsub(EVERYTHING_AFTER_SCRIPT, '')
  end
end
