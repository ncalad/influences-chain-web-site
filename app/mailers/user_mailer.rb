class UserMailer < ApplicationMailer
  def welcome(user)
    @user = user
    mail(to: @user.email, subject: 'Welcome')
  end

  def password_reset(user, subdomain)
    @user = user
    @url = edit_password_reset_url(@user.password_reset_token, subdomain: subdomain)
    send_mail_with_locale(@user, 'Password Reset')
  end

  def send_password_request(user)
    @user = user
    @url = new_password_changes_url(token: user.password_token, subdomain: :siteadmin)
    send_mail_with_locale(@user, 'Please set password')
  end

  def influencer_registration(user, campaign)
    @user = user
    subdomain = campaign.tenant&.name
    @url = new_password_changes_url(token: user.password_token, subdomain: subdomain)
    send_mail_with_locale(@user, 'You have been added to campaign. Please set password')
  end

  def campaign_invitation(user, campaign)
    @user = user
    subdomain = campaign.tenant&.name
    @welcome_text = campaign.welcome_text
    @url = campaign_url(campaign, subdomain: subdomain)
    send_mail_with_locale(@user, 'You have been added to campaign. Please set password')
  end

  def contact_invitation(user)
    @user = user
    @url = new_password_changes_url(token: @user.password_token, subdomain: contact_subdomain)
    send_mail_with_locale(@user, "You have been added to #{parent_name} - #{parent_title}. Please set password")
  end

  def ticket_reply(reply)
    @ticket = reply.ticket
    @reply = reply
    user = User.find_by('lower(email) = ?', @ticket.email)
    translate = user&.translate_language || I18n.default_locale
    I18n.with_locale(translate) do
      mail to: @ticket.email, subject: "Support reply on Ticket #{@ticket.id}"
    end
  end

  def admin_ticket_notification(admin, ticket)
    @ticket = ticket
    send_mail_with_locale(admin, 'New ticket')
  end

  def new_state(campaign_influencer, subdomain)
    @c_i = campaign_influencer
    @url = campaign_url(@c_i.campaign, subdomain: subdomain)
    send_mail_with_locale(@c_i.influencer, 'New status')
  end

  private

  def parent_name
    @user.parent&.class&.name
  end

  def parent_title
    parent_name == 'Tenant' ? @user.parent.name : @user.parent.project_name
  end

  def contact_subdomain
    parent_name == 'Tenant' ? @user.parent&.name&.downcase : @user.parent&.tenant&.name
  end

  def send_mail_with_locale(user, subject)
    I18n.with_locale(user.translate_language) do
      mail to: user.email, subject: subject
    end
  end
end
