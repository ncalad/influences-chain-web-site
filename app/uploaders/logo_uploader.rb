class LogoUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  version :small do
    resize_to_fit(100, 100)
  end

  version :normal do
    resize_to_fill(206, 206)
  end

  version :middle do
    resize_to_fill(300, 300)
  end

  def public_id
    self.file.original_filename
  end
end
