class CampaignUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  def public_id
    self.file.original_filename
  end
end
