class Tenant < ApplicationRecord
  acts_as_paranoid

  belongs_to :owner, class_name: User, foreign_key: :user_id
  has_many :contacts, foreign_key: :parent_id, class_name: 'User::TenantContact'
  has_many :campaigns
  has_many :active_campaigns, -> { where('end_date >= ?', Date.current) }, class_name: Campaign
  has_many :completed_campaigns, -> { where('end_date < ?', Date.current) }, class_name: Campaign
  has_many :tickets

  before_save :name_format

  mount_base64_uploader :logo, TenantLogoUploader

  def subdomain
    name.downcase.gsub(' ', '-')
  end

  private

  def name_format
    name.squish!
  end
end
