class Campaign < ApplicationRecord
  acts_as_paranoid

  enum location: ['Mexico', 'Brazil', 'Spain', 'Portugal']
  enum tenant_logo_placement: ['Top Left', 'Top Center', 'Top Right']

  belongs_to :owner, class_name: User, foreign_key: :user_id
  belongs_to :tenant
  has_many :contacts, foreign_key: :parent_id, class_name: 'User::CampaignContact'
  has_many :campaigns
  has_many :logos, ->{ order(placement: :asc) }
  has_many :campaign_influencers
  has_many :influencers, through: :campaign_influencers
  has_many :tasks
  has_one :forum

  accepts_nested_attributes_for :logos, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :forum, reject_if: :without_forum

  mount_base64_uploader :contract, CampaignUploader

  private

  def without_forum(attrs)
    !use_forum || attrs['title'].blank?
  end
end
