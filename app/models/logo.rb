class Logo < ApplicationRecord
  enum placement: ['Top Left', 'Top Center', 'Top Right']

  belongs_to :campaign, optional: true

  mount_uploader :image, LogoUploader
end
