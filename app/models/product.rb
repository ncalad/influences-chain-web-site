class Product < ApplicationRecord
  belongs_to :forum
  belongs_to :user
  has_many :categories
end
