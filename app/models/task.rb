class Task < ApplicationRecord
  acts_as_paranoid

  enum task_type: %w(basic survey)

  belongs_to :owner, class_name: User, foreign_key: :user_id

  has_many :task_users
  has_many :users, through: :task_users

  scope :by_type, ->(user, type){ joins(:task_users).where('task_users.user_id = ?', user.id).group('id') if type == 'completed' }
end
