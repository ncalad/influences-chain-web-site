class Topic < ApplicationRecord
  belongs_to :user
  belongs_to :category
  belongs_to :referred, class_name: 'Topic', foreign_key: 'referred_id'
  has_many :subtopics, class_name: 'Topic', foreign_key: 'referred_id'
  has_many :posts
end
