class User < ApplicationRecord
  TEXT_FIELDS = %w(first_name surname email matrimonial_surname profession personality_type first_address second_address third_address city interior).freeze
  BOOLEAN_FIELDS = %w(key_influencer friends_and_family).freeze
  INTEGER_FIELDS = %w(childrens postal_code).freeze
  ENUMS_FIELDS = %w(gender marital_status country).freeze

  include AuthenticateUser
  include Password

  acts_as_paranoid

  enum locale: [:en, :es, :pt]
  enum primary_language: [:English, :Spanish, :Portuguese]
  enum phone_type: [:Mobile, :Home, :Office]
  enum gender: [:Male, :Female, :Other, :Unspecified]
  enum marital_status: [:Single, :Married, :Divorced, :Separated, :Cohabiting]
  enum country: [:Mexico, :Brazil, :Spain, :Portugal]

  validates :email, presence: true, uniqueness: { scope: :type }

  has_many :tenants
  has_many :campaign_influencers
  has_many :connected_campaigns, through: :campaign_influencers, source: :campaign
  has_many :replies
  has_many :social_networks

  accepts_nested_attributes_for :social_networks, allow_destroy: true, reject_if: :all_blank

  mount_base64_uploader :avatar, LogoUploader

  scope :search_influencers, lambda { |params|
    where(search_by_text(params))
      .where(search_by_numbers(params))
      .where(search_by_booleans(params))
      .where(search_by_enums(params))
  }

  def superadmin?
    false
  end

  def campaign_admin?
    false
  end

  def influencer?
    true
  end

  def tenant_contact?
    false
  end

  def campaign_contact?
    false
  end

  def change_locale(language)
    update(primary_language: language) if User.primary_languages.keys.include?(language)
  end

  def change_password(params)
    assign_attributes(password_params(params[:password]))
    encrypt_password
    save
  end

  def send_reset_password(subdomain)
    self.password_reset_token = SecureRandom.urlsafe_base64
    self.password_reset_sent_at = Time.zone.now
    save!
    SendResetPasswordMailJob.set(wait: 5.seconds).perform_later(self, subdomain)
  end

  def full_name
    [first_name, surname, matrimonial_surname].join(' ')
  end

  def full_surname
    [surname, matrimonial_surname].join(' ')
  end

  def after_password_set_url
    nil
  end

  def type_name
    self.class.name.demodulize.parameterize
  end

  def translate_language
    I18n.t(".translates.#{primary_language}", default: nil) || I18n.default_locale
  end

  private

  def password_params(pass)
    {
      password: pass,
      password_token: nil,
      password_token_expires_at: nil
    }
  end

  def self.search_by_text(params)
    params.keys.map { |k| "#{k} ilike '%#{params[k]}%'" if TEXT_FIELDS.include?(k) && params[k].present? }.compact.join(' AND ')
  end

  def self.search_by_numbers(params)
    params.keys.map { |k| "#{k} = #{params[k]}" if INTEGER_FIELDS.include?(k) && params[k].present? }.compact.join(' AND ')
  end

  def self.search_by_booleans(params)
    params.keys.map { |k| "#{k} = #{!params[k].to_i.zero?}" if BOOLEAN_FIELDS.include?(k) && params[k].present? && params[k].to_i == 1 }.compact.join(' AND ')
  end

  def self.search_by_enums(params)
    params.keys.map { |k| "#{k} = #{User.send(k.pluralize)[params[k].to_s]}" if ENUMS_FIELDS.include?(k) && params[k].present? }.compact.join(' AND ')
  end

  def send_registration_for_contact
    generate_password_token
    UserMailer.contact_invitation(self).deliver!
  end
end
