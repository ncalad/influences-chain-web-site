class User::CampaignAdmin < User
  def campaign_admin?
    true
  end

  def influencer?
    false
  end
end
