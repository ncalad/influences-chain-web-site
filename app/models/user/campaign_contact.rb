class User::CampaignContact < User
  after_create :send_registration_for_contact

  belongs_to :parent, foreign_key: :parent_id, class_name: Campaign

  def campaign_admin?
    false
  end

  def influencer?
    false
  end

  def campaign_contact?
    true
  end

  def after_password_set_url
    Rails.application.routes.url_helpers.new_sessions_path(subdomain: parent&.tenant&.name)
  end
end
