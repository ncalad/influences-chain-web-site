class User::Influencer < User
  def self.import(file)
    spreadsheet = Roo::Spreadsheet.open(file.path)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      influencer = find_by(email: row['EMAIL ADDRESS']) || new
      influencer.attributes = imported_params(row)
      influencer.save!
    end
  end

  def self.imported_params(data)
    {
      username: data['USER NAME'],
      first_name: data['FIRST NAME'],
      surname: data['PATRIMONIAL SURNAME'],
      matrimonial_surname: data['MATRIMONIAL SURNAME'],
      primary_language: User.primary_languages[data['PRIMARY LANGUAGE'].capitalize],
      email: data['EMAIL ADDRESS'],
      birth_year: data['YEAR OF BIRTH'],
      gender: User.genders[data['GENDER'].capitalize]
    }
  end

  def after_password_set_url
    Rails.application.routes.url_helpers.new_sessions_path
  end
end
