class Admin::InfluencersImportsController < AdminsController
  def create
    User::Influencer.import(influencers_import_params[:file])

    redirect_to admin_influencers_path
  end

  private

  def influencers_import_params
    params.require(:influencers_import).permit(:file)
  end
end
