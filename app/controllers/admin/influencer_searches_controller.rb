class Admin::InfluencerSearchesController < AdminsController
  expose(:campaign)
  expose(:influencer_search) { InfluencerSearch.new }
  expose(:result) { User::Influencer.search_influencers(params['influencer_search']) }
end
