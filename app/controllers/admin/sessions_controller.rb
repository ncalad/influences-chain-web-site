class Admin::SessionsController < AdminsController
  skip_before_action :require_admin
  before_action :check_login, only: :new

  expose(:user) { User.authenticate(session_params.dig(:email), session_params.dig(:password)) }
  expose(:new_session) { Session.new(session_params) }

  def create
    user ? session[:user_id] = user.id : flash[:error] = 'Email or password is wrong'
    redirect_to root_path
  end

  def destroy
    reset_session
    redirect_to root_path
  end

  private

  def session_params
    params.require(:session).permit! if params[:session]
  end

  def check_login
    redirect_to root_path if current_user
  end
end
