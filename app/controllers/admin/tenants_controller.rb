class Admin::TenantsController < AdminsController
  expose(:all_tenants) { Tenant.all }
  expose(:tenant, scope: ->{ all_tenants })
  expose(:contacts) { tenant.contacts }
  expose(:campaigns) { tenant.campaigns }

  def update
    create
  end

  def create
    if tenant.update(tenant_params)
      redirect_to admin_tenants_path
    else
      flash[:error] = tenant.errors.messages
      redirect_to :back
    end
  end

  def destroy
    tenant.destroy
    redirect_to admin_tenants_path
  end

  private

  def tenant_params
    params.require(:tenant).permit!
  end
end
