class Admin::TasksController < AdminsController
  expose(:campaign)
  expose(:tasks) { campaign.tasks }
  expose(:task, parent: :campaign)

  def update
    create
  end

  def create
    if task.update(task_params)
      redirect_to admin_campaign_tasks_path
    else
      flash[:error] = task.errors.messages
      redirect_to :back
    end
  end

  def destroy
    task.destroy
    redirect_to admin_campaign_tasks_path
  end

  private

  def task_params
    params.require(:task).permit(:name, :summary, :description, :task_type, :user_id, :survey, :due)
  end
end
