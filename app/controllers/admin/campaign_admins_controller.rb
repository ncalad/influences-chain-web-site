class Admin::CampaignAdminsController < AdminsController
  expose(:campaign_admin, model: User::CampaignAdmin)
  expose(:campaign_admins) { User::CampaignAdmin.all }

  def update
    create
  end

  def create
    if campaign_admin.update(admin_params)
      redirect_to admin_campaign_admins_path
    else
      flash[:error] = campaign_admin.errors.messages
      redirect_to :back
    end
  end

  def destroy
    campaign_admin.destroy
    redirect_to admin_campaign_admins_path
  end

  private

  def admin_params
    params.require(:user_campaign_admin).permit! if params[:user_campaign_admin]
  end
end
