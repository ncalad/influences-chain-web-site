class AdminsController < ApplicationController
  before_action :require_admin

  def require_admin
    redirect_to new_admin_session_url(subdomain: :siteadmin) if !current_user&.superadmin? && !current_user&.campaign_admin?
  end
end
