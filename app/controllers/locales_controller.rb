class LocalesController < ApplicationController
  def update
    session[:locale] = I18n.t(".translates.#{params[:language]}", default: nil)
    subdomain == 'siteadmin' ? current_user&.change_locale(params[:language]) : current_tenant_user&.change_locale(params[:language])
    redirect_to :back
  end
end
