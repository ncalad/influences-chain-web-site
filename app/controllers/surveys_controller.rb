class SurveysController < ApplicationController
  expose(:task)

  after_action :completed_task

  private

  def completed_task
    task.users << current_tenant_user
  end
end
