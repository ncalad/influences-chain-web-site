class ProductsController < ApplicationController
  before_action :require_tenant_login

  expose(:product)
  expose(:forum)
  expose(:campaign) { forum.campaign }
  expose(:all_categories) { product.categories }

  layout 'tenant'

  def create
    head :no_content unless product.update(product_params)
  end

  def destroy
    product.destroy
  end

  private

  def product_params
    params.require(:product).permit(:title, :user_id).merge(forum_id: params[:forum_id])
  end
end
