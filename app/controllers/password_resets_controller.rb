class PasswordResetsController < ApplicationController
  before_action :check_passwords_eq, only: :update
  before_action :password_reset_expiry, only: :update

  expose :user, :find_user

  def create
    user&.send_reset_password(request.subdomains[0])
    redirect_to user ? root_path : new_password_reset_path
  end

  def update
    user&.change_password(params)
    redirect_to root_path
  end

  private

  def find_user
    params[:password_reset] ? User.find_by("lower(email) = ?", params[:password_reset][:email].downcase) : User.find_by(password_reset_token: params[:id])
  end

  def password_reset_expiry
   return if user && user.password_reset_sent_at > 1.hour.ago
   flash[:error] = I18n.t('.password_expired')
   redirect_to new_password_reset_path
  end
end
