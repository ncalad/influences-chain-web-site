class TicketsController < ApplicationController
  expose(:ticket)

  def create
    ticket.update(ticket_params)
    flash[:notice] = I18n.t('.tickets.sent')
    redirect_to root_path
  end

  private

  def ticket_params
    params.require(:ticket).permit(:email, :text).merge(tenant: tenant)
  end

  def tenant
    @tenant ||= Tenant.where('lower(name) = lower(?)', request.subdomains[0]).first
  end
end
