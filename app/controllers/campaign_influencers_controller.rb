class CampaignInfluencersController < ApplicationController
  expose(:campaign_influencer)

  def update
    head :no_content unless campaign_influencer.update(campaign_influencer_params)
  end

  private

  def campaign_influencer_params
    params.require(:campaign_influencer).permit(:contract)
  end
end
