class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale

  def current_user
    @user ||= User.find(session[:user_id]) if session[:user_id]
  rescue ActiveRecord::RecordNotFound
    reset_session
  end
  helper_method :current_user

  def tenant
    @tenant ||= Tenant.where('lower(name) = ?', request.subdomains(0).first.downcase.gsub('-', ' ')).first
  end
  helper_method :tenant

  def campaign
    @campaign ||= Campaign.includes(:logos).find(params[:campaign_id]) if params[:campaign_id]
  end
  helper_method :campaign

  def campaign_influencer
    @campaign_influencer ||= campaign.campaign_influencers.find_by(influencer: current_tenant_user)
  end
  helper_method :campaign_influencer

  def forum
    @forum ||= campaign.forum if campaign.forum
  end
  helper_method :forum

  def product
    @product ||= Product.find(params[:product_id]) if params[:product_id]
  end
  helper_method :product

  def category
    @category ||= Category.find(params[:category_id]) if params[:category_id]
  end
  helper_method :category

  def topic
    @topic ||= Topic.find(params[:topic_id]) if params[:topic_id]
  end
  helper_method :topic

  def current_tenant_user
    @current_tenant_user ||= User.find(session["#{tenant&.name}_user_id"]) if session["#{tenant&.name}_user_id"]
  rescue ActiveRecord::RecordNotFound
    reset_session
  end
  helper_method :current_tenant_user

  def require_tenant_login
    redirect_to new_sessions_url(subdomain: tenant.subdomain) if session["#{tenant.name}_user_id"].nil?
  end

  def subdomain
    request.subdomains(0).first.downcase
  end

  protected

  def admin_authenticate
    return true if Rails.env.development? || Rails.env.test?
    authenticate_or_request_with_http_basic do |username, password|
      username == ENV['ADMIN_USERNAME'] && password == ENV['ADMIN_PASSWORD']
    end
  end

  private

  def set_locale
    I18n.locale = user_locale ? I18n.t(".translates.#{user_locale}", default: nil) : session[:locale] || I18n.default_locale
  end

  def user_locale
    (subdomain == 'siteadmin' ? current_user : current_tenant_user)&.primary_language
  end

  def check_passwords_eq
    return if params[:password] == params[:password_confirmation] && !params[:password].blank?
    flash[:error] = I18n.t('.password_not_eq')
    redirect_to :back
  end
end
