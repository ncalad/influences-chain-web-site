$('#post_modal button.close').click()
$('#post_modal form.post')[0].reset()
$('.posts').append("<%= j(render 'row', post: post) %>")
$('html, body').animate
  scrollTop: $("[data-post-id=#{<%= post.id %>}]").offset().top
