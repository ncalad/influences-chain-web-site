$('#reply_modal').remove()

$('.tickets').prepend("<%= j(render 'shared/modal', id: 'reply_modal', parent_id: params[:ticket_id], title: I18n.t('.tickets.reply'), form: 'admin/replies/form') %>")

$('#reply_modal').modal('show')
