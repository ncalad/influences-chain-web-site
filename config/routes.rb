Rails.application.routes.draw do
  constraints(subdomain: /siteadmin/) do
    ActiveAdmin.routes(self)

    namespace :admin do
      resource :session, only: %w(new create destroy)
      resources :dashboard, only: :index
      resources :campaign_admins
      resources :tenants
      resources :campaigns do
        resources :campaign_influencers
        resource :influencer_searches, only: %w(new create)
        resources :tasks
      end
      resources :influencers
      resources :influencers_imports, only: %i(index create)

      scope '/:type/:parent_id', constraints: { type: /(campaign|tenant)/ } do
        resources :contacts
      end

      resources :tickets, only: %w(index update) do
        resources :replies, only: %w(new create)
      end
    end

    root to: 'admin/dashboard#index'
  end

  constraints(subdomain: /^(?!siteadmin)(\w+)/) do
    resources :tenants
    resource :sessions, only: %w(new create destroy)

    root to: 'tenants#index'

    resources :campaigns do
      resource :forum, only: :show
      resources :tasks, path: 'tasks/:type', constraints: { type: /(uncompleted|completed)/ }, defaults: { type: 'uncompleted' } do
        resource :survey, only: :show
        resource :task_users, only: :create
      end
    end

    resources :forum, only: [] do
      resources :products
    end

    resources :products, only: [] do
      resources :categories
    end

    resources :categories, only: [] do
      resources :topics
    end

    resources :topics, only: [] do
      resources :posts
    end

    resources :campaign_influencers, only: %w(edit update)
  end

  resource :locales, only: :update
  resource :password_changes, only: %w(new edit update create)
  resources :password_resets, except: %w(index destroy)
  resources :tickets, only: %w(new create)
  resources :avatars, only: %w(new update)
end
